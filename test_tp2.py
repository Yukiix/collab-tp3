#on veut pouvoir créer des boites
from tp2 import *
def test_box_create():
    b=Box()

#on veut pouvoir mettre des trucs dedans
def test_box_add():
    b=Box()
    b.add("truc1")
    b.add("truc2")

#on veut que nos attribus soient privées
def test_box_prive():
    b=Box()
    assert b._contents==[] # ????

#on veut pouvoir écrire truc1 in b
def test_appartenance():
    b=Box()
    b.add("truc1")
    assert "truc1" in b
    assert "truc2" not in b

#on veut pouvoir retirer un truc d'une boite
def test_remove():
    b=Box()
    b.add("truc1")
    assert "truc1" in b
    b.remove("truc1")
    assert "truc1" not in b

#on veut pouvoir ouvrir et fermé notre boite, ainsi que savoir si elle est ouverte ou pas
def test_ouverture():
    b=Box()
    assert b.is_open()==False
    b.open()
    assert b.is_open()==True
    b.close()
    assert b.is_open()==False

#on veut qu'une boite permette b.action_look pour savoir ce qu'elle contient uniquement si elle est ouverte
def test_action_look():
    b=Box()
    b.add("truc")
    b.add("machin")
    assert b.action_look()=="la boite est fermee"
    b.open()
    assert b.action_look()=="la boite contient: truc, machin", b.action_look()

#On veut créer des chose qui prennent de la place
def test_chose():
    t=Thing(3)

#On veut pouvoir connaitre la place d'une chose
def test_volume_chose():
    t=Thing(3)
    assert t.volume()==3

#On veut pouvoir manipuler la capaciter d'une boite
def test_capacity():
    b=Box()
    b.set_capacity(5)
    assert b.capacity()==5

#On voudrait tester si il reste de la place pour une nouvelle chose
def test_has_room_for():
    b=Box()
    t=Thing(3)
    assert b.has_room_for(t)==True
    b.set_capacity(2)
    assert b.has_room_for(t)==False

#On voudrait une methode action add qui mette quelque chose dans une boite si il y a asser de place
def test_action_add():
    b=Box()
    t=Thing(3)
    b.open()
    b.set_capacity(4)
    b.action_add(t)
    assert b.capacity()==1
    b.action_add(t)
    assert b.capacity()==1
    assert b.action_add(t)==False, b.action_add(t)
    b.set_capacity(4)
    assert b.action_add(t)==True
    b.close()
    assert b.action_add(t)==False


#On voudrait pouvoir donner un nom à une chose et pouvoir reconnaitre cette chose avec son nom
def test_set_name():
    T=Thing(10)
    T.set_name("bidule")
    assert repr(T)=="bidule"

#On voudrait tester si une chose a un nom donner
def test_has_name():
    T=Thing(10)
    T.set_name("bidule")
    assert T.has_name("bidule")
    assert not T.has_name("lol")

#On voudrait pouvoir trouver un objet dans une boite à partir de son nom
def test_find():
    B=Box()
    T=Thing(3)
    T.set_name("bidule")
    B.set_capacity(10)
    B.open()
    B.action_add(T)
    assert B.find("bidule")==T, print(T)
    assert B.find("truc")==None
    B.close()
    assert B.find("bidule")==None

#On veut pouvoir préciser les parametres de nos objets directement dans le constructeur de maniere optinonnelle
def test_constructeur():
    B=Box()
    assert not B.is_open()
    B1=Box(is_open=True, capacity=10)
    assert B1.is_open()
    assert B1.capacity()==10
    T=Thing(2)
    assert not T.has_name("bidule"), print(T)
    T1=Thing(2, "bidule")
    assert T1.has_name("bidule")


#Exo 8
import yaml
import io


def test_creation_boite_yaml():
    test="""
    - is_open: true
      capacity: 3
    - is_open: false
      capacity: 5
    """
    s=io.StringIO(test)
    liste=yaml.load(s)
    assert liste==[{'capacity': 3, 'is_open': True},{'capacity': 5, 'is_open': False}], print(liste)
    #on obtient notre liste de boite au format yaml
    liste_boite=[]  # cette fois on créer une liste d'objet
    for dico in liste:
        x=Box.from_data(dico)
        liste_boite.append(x)
    assert liste_boite[0].is_open()
    assert liste_boite[1].capacity()==5

def test_creation_thing_yaml():
    test="""
    - volume: 10
      nom: "bidule"
    - volume: 3
      nom: "baleine"
    - volume: 8000
      nom: "ocean"
    """
    s=io.StringIO(test)
    liste=yaml.load(s)
    assert liste==[{'volume': 10, 'nom': 'bidule'}, {'volume': 3, 'nom': 'baleine'}, {'volume': 8000, 'nom': 'ocean'}], print(liste)
    #on obtient notre liste de thing au format yaml ( liste de dico avec pour clé les attribus de la boite )
    liste_thing=[]
    for dico in liste:
        x=Thing.from_data(dico)
        liste_thing.append(x)
    assert liste_thing[0].has_name("bidule")
    assert not liste_thing[1].has_name("ocean")
    assert liste_thing[2].volume()==8000
    liste_thing[1].set_name("ocean")
    assert liste_thing[1].has_name("ocean")


def test_creation_boite_et_thing_from_data():
    test="""
    - type: Box
      is_open: true
      capacity: 3

    - type: Box
      is_open: false
      capacity: 5

    - type: Thing
      volume: 10
      nom: "bidule"

    - type: Thing
      volume: 3
      nom: "baleine"

    - type: Thing
      volume: 8000
      nom: "ocean"
    """
    s=io.StringIO(test)
    liste=yaml.load(s)
    assert liste==[{'type': 'Box', 'is_open': True, 'capacity': 3}, {'type': 'Box', 'is_open': False, 'capacity': 5}, {'type': 'Thing', 'volume': 10, 'nom': 'bidule'}, {'type': 'Thing', 'volume': 3, 'nom': 'baleine'}, {'type': 'Thing', 'volume': 8000, 'nom': 'ocean'}], print(liste)
    liste_objet=liste_from_yaml(liste)
    assert liste_objet[0].is_open()
    assert not liste_objet[1].is_open()
    assert liste_objet[2].volume()==10
    assert liste_objet[3].has_name("baleine")
    assert liste_objet[4].has_name("ocean")
    liste_objet[1].open()
    liste_objet[1].action_add(liste_objet[3])
    assert liste_objet[1].find("baleine").volume()==3


def test_setkey_openwith():
    B=Box()
    assert not B.is_open()
    Clef=Thing(1, "clef")
    B.set_key(Clef)
    B.open_with(Clef)
    assert B.is_open()
