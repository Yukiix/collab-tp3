import yaml

class Personne:
    def __init__(self, nom, prenom, adresse):
        self._nom = nom
        self._prenom = prenom
        self._adresse = adresse

    def __repr__(self):
        return self._nom + " " + self._prenom

    def get_adresse(self):
        return self._adresse


    @staticmethod
    def from_file(nomfic):
        res=[]
        with open(nomfic, "r") as flux:
            personnes = yaml.load(flux)
        for p in personnes:
            nom = p.get("nom")
            prenom = p.get("prenom")
            adresse = p["adresse"]
            res.append(Personne(nom, prenom, adresse))
        return res


lp=Personne.from_file("contacts.yaml")
for p in lp:
    print(p)
    print(p.get_adresse())
