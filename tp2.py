class Box:
    def __init__(self, is_open=False, capacity=None):
        self._contents = []
        self._ouvert=is_open
        self._capacity=capacity
        self._key=None

    def add(self, truc):
        self._contents.append(truc)

    def __contains__(self, machin):
        if machin in self._contents:
            return True
        return False

    def remove(self, truc):
        self._contents.remove(truc)

    def is_open(self):
        return self._ouvert

    def open(self):
        self._ouvert=True
        
    def open_with(self, cle):
        if cle == self._key:
            self._ouvert=True

    def close(self):
        self._ouvert=False

    def action_look(self):
        if self.is_open():
            res="la boite contient: "
            res+=", ".join(self._contents)
        else:
            res="la boite est fermee"
        return res

    def set_capacity(self, x):
        self._capacity=x

    def capacity(self):
        return self._capacity

    def has_room_for(self, t):
        if self.capacity()==None or self.capacity()>t.volume():
            return True
        return False

    def action_add(self, t):
        if self.has_room_for(t) and self.is_open():
            self.add(t)
            self.set_capacity(self.capacity()-t.volume())
            return True
        return False

    def find(self, n):
        if self.is_open():
            for thing in self._contents:
                if thing.has_name(n):
                    return thing
        return None

    def set_key(self, cle):
        self._key=cle


    @staticmethod
    def from_data(data):
        capacity = data.get("capacity", None)
        isopen = data.get("is_open", False)
        return Box(is_open=isopen, capacity=capacity)

class Thing:
    def __init__(self, x, nom=None):
        self._volume = x
        self._name=nom

    def __repr__(self):
        return self._name

    def __eq__(self, thing):
        if self._volume==thing.volume() and thing.has_name(self._name):
            return True
        return False

    def volume(self):
        return self._volume

    def set_name(self, n):
        self._name=n

    def has_name(self, n):
        return self._name==n

    @staticmethod
    def from_data(data):
        volume = data.get("volume", 0)
        nom = data.get("nom", None)
        return Thing(volume, nom=nom)



def liste_from_yaml(liste):
    res=[]
    for dico in liste:
        if "type" in dico.keys():
            if dico["type"]=="Box":
                res.append(Box.from_data(dico))
            elif dico["type"]=="Thing":
                res.append(Thing.from_data(dico))
    return res
